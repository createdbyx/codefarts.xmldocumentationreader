/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.XMLDocumentationReader
{
    using System.Collections.Generic;

    /// <summary>
    /// Provides a member model that contains information about the member.
    /// </summary>
    public class MemberModel
    {
        /// <summary>
        /// Gets or sets the fully qualified name of the member.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the member name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the summery documentation for the member.
        /// </summary>
        public string Summery { get; set; }

        /// <summary>
        /// Gets or sets the returns documentation for the member.
        /// </summary>
        public string Returns { get; set; }

        /// <summary>
        /// Gets or sets the remarks documentation for the member.
        /// </summary>
        public string Remarks { get; set; }
        
        /// <summary>
        /// Gets or sets the type of member.
        /// </summary>
        /// <remarks>Types are Method, Property, Field, Event etc.</remarks>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets a dictionary containing the parameter documentation.
        /// </summary>
        public IDictionary<string, string> Parameters { get; set; }

        /// <summary>
        /// Gets or sets the parent model.
        /// </summary>
        public MemberModel Parent { get; set; }

        /// <summary>
        /// Gets or sets a dictionary containing the type parameter documentation.
        /// </summary>
        public IDictionary<string, string> TypeParameters { get; set; }
    }
}